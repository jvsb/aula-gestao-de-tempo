Após a aula de Gestão de tempo preferi adotar o método de Rotina
para melhorar o meu desempenho nos estudos e me organizar melhor,
O dia de hoje não foi muito diferente e vou manter o mesmo para o dia seguinte.

Rotina para os dias de semana: (No momento estou em Mato Grosso do Sul, com o fuso horário de 1h a menos)

08:10- Despertar
08:20- Tomar café da manhã
08:30- Escovar os dentes
08:55- Se preparar para aula EAD
09:00/11:00-Aula EAD
11:00/12:30-Almoço/Descanço/video jogos
12:30/12:55-Preparar para EAD da tarde
13:00/15:00-Aula EAD
15:00/16:00-Descanço/video jogos
16:00/16:15-Banho
16:15/21:00-Revisar/Aplicar os conhecimentos aprendidos
21:00/23:00-Assistir Séries/Filmes/Animes com a namorada
23:00- Mimir

Rotina para os finais de semana:

10:30- Despertar
10:40- Tomar café da manhã
10:50- Escovar os dentes
11:00- Começar a preparar o almoço
12:30- Almoçar
13:00/16:00- Ver o cblol
16:00/16:15-Banho
16:15/20:00- Revisar as matérias que tive dificuldade/aprimorar os conhecimentos
20:01/01:00- Gameplay elevada

Acredito que aplicando uma rotina balanceada poderá atingir resultados notaveis.

Objetivos semanais:
-Ter uma meta de estudo de 20h Semanais:
-Conseguir ser mais prático nos momentos de estudo
-Manter uma alimentação saudável
-Respeitar o tempo de cada horário que foi proposto

Objetivos Mensais:
-Planejar algum projeto na área desenvolvimento aprendida
-Terminar de ler Stone Ocean
-Arrumar o meu Computador
-Manter a rotina de estudo

